# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
posts = Post.create([{ title: 'First Post', body: 'This is the first blog post.' },
                     { title: 'Second Post', body: 'This is the second post to the blog.' }])
comments = Comment.create([{ body: 'First comment on first post.', post_id: '1' },
                           { body: 'First comment on second blog post.', post_id: '2'},
                           { body: 'Another comment on the first blog post.', post_id: '1' }])
