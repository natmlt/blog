Coursera class: Web Application Architectures
-------------

The course description is [here]. (https://www.coursera.org/course/webapplications)

---------------------------------------
### Specifications:
A list of some of the specifications for the Blog application to be implemented over the period of the course:

Administrator functionality:

  * Create new posts.
  * Edit posts.
  * Delete posts.
  * Delete user comments.

User functionality:

  * View posts (typically most recent posts displayed first).
  * Comment on posts (comments linked to posts).
  * Not allowed to edit/delete other user's comments.
  * Not allowed to edit/delete posts.

---------------------------------------
### Assignment (Week 2):

1. Modify Post model to define relationship with Comments.
   * has_many added to Post with cascading deletes using destroy syntax
   * belongs_to added to Comment
2. Create nested resource and confirm their existence via the command 'rake routes'.
   * Note: the relationships work from the console but not in a browser without additional code modifications.
3. Edit Post and Comment models to add validation testing for the presence of all four fields.

---------------------------------------
### Assignment (Week 1):

1. Blog application initialized 
2. Two Sqlite table models created:
    * posts:
        - title: string
        - body: text
    * comments:
        - post_id: int
        - body: text
3. Scaffolding generated
4. Project push to BitBucket

---------------------------------------
The commands used to create the Post and Comment models, views, and controllers:  

Create the Post object and initialize database table:

    rails generate scaffold Post title:string body:text
    rake db:migrate

Create the Comment object and initialize database table (see note first):

    rails generate scaffold Comment post_id:integer body:text
    rake db:migrate
  
  
*NOTE:*  
I found another way to create the post_id so to test it I decided to created two branches.  It seemed like the easiest way to see the potential difference in specifying the post_id vs using the reference command.  

I add extra work to manually enter the post_id field of the comments table unless it's not going to be a foreign key but then it should be named differently.  Rails can create the relationship for us with this command:  

    rails generate scaffold Comment body:text post:references
    rake db:migrate

The reference commands biggest difference is the addition of the index:

    add_index "comments", ["post_id"], name: "index_comments_on_post_id"

Without using reference the index will have to be created another way but doing that opens up the option for the instructors to explain the reason for it.  I suspect that is what will occur in the next couple of weeks.



